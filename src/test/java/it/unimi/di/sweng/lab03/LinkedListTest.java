package it.unimi.di.sweng.lab03;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class LinkedListTest {

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private IntegerList list;
	
	@Before
	public void setUp(){
		list = new IntegerList();
	}
	@Test
	public void firstTestToBeReplaced() {
		assertThat(list.toString()).isEqualTo("[]");
	}
	@Test
	public void testAddLast() {
		list.addLast(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addLast(3);
		assertThat(list.toString()).isEqualTo("[1 3]");
	}
	@Test
	public void testParametrizedConstructor() {
		list = new IntegerList("");
		assertThat(list.toString()).isEqualTo("[]");
		list = new IntegerList("1");
		assertThat(list.toString()).isEqualTo("[1]");
		list = new IntegerList("1 2 3");
		assertThat(list.toString()).isEqualTo("[1 2 3]");
	}
	@Test(expected=IllegalArgumentException.class)
	public void illegalArgumentTest(){
		list = new IntegerList("1 2 aaa");
	}
	@Test
	public void testAddFisrt() {
		list.addFirst(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addFirst(3);
		assertThat(list.toString()).isEqualTo("[3 1]");
	}
	@Test
	public void testRemoveFisrt() {
		list = new IntegerList("1 2");
		boolean bool;
		bool = list.removeFirst();
		assertThat(bool == true);
		assertThat(list.toString()).isEqualTo("[2]");
		bool = list.removeFirst();
		assertThat(bool == true);
		assertThat(list.toString()).isEqualTo("[]");
		bool = list.removeFirst();
		assertThat(bool == false);
	}
	@Test
	public void testRemoveLast() {
		list = new IntegerList("7 10");
		boolean bool;
		bool = list.removeLast();
		assertThat(list.toString()).isEqualTo("[7]");
		bool = list.removeLast();
		assertThat(list.toString()).isEqualTo("[]");
		bool = list.removeLast();
		assertThat(list.toString()).isEqualTo("[]");
	}
}
