package it.unimi.di.sweng.lab03;

public class IntegerNode {
	private int value;
	private IntegerNode next;
	
	public IntegerNode(int value) {
		this.value = value;
		next = null;
	}

	public IntegerNode getNext() {
		return next;
	}

	public void setNext(IntegerNode node) {
		next = node;
	}
	
	public int getValue() {
		return value;
	}
	
}
