package it.unimi.di.sweng.lab03;

public class IntegerList {
	private IntegerNode head;
	private IntegerNode tail;
	
	public IntegerList(String intList) {
		String[] list = intList.split(" ");
		for(String value : list){
			if(value != "")
				addLast(Integer.parseInt(value));
		}
	}

	public IntegerList() {
		head = tail = null;
	}

	public String toString(){
		StringBuilder result= new StringBuilder("[");
		IntegerNode current = head;
		while(current != null){
			if(!isHead(current))
				result.append(" ");
			result.append(current.getValue());
			current = current.getNext();
		}
		result.append("]");
		return result.toString();
	}
	private boolean isHead(IntegerNode node){
		return node == head;
	}
	
	public void addLast(int value) {
		IntegerNode newNode = new IntegerNode(value);
		if(head == null)
			head = tail = newNode;
		else{
			tail.setNext(newNode);
			tail = newNode;
		}
	}

	public void addFirst(int value) {
		IntegerNode newNode = new IntegerNode(value);
		newNode.setNext(head);
		head = newNode;
	}

	public boolean removeFirst() {
		if(head==null)
			return false;
		else{
			head = head.getNext();
			return true;
		}	
	}

	public boolean removeLast() {
		if(head == null)
			return false;
		else if(head==tail){
			head = tail = null;
			return true;
		}
		else{
			tail = this.getSecondLast();
			tail.setNext(null);
			return true;
		}
	}
	
	public IntegerNode getSecondLast(){
		IntegerNode current = head;
		IntegerNode prec=head;
		while(current.getNext() != null){
			prec = current;
			current = current.getNext();
		}
		return prec;
	}
}
